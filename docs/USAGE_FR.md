# Webservice ph5-availability

## Description

Ce service donne la disponibilité des enregistrements sismiques dans la base de données PH5. Il fournit des informations sous forme de plages temporelles détaillées.

Il y a deux méthodes de requêtes pour ce service :

/extent

Produis une liste des plages temporelles disponibles selon les canaux (network, station, location, channel, quality) et l'intervalle de temps demandés.

/query

Produis une liste des plages temporelles continues disponibles selon les canaux (network, station, location, channel, quality) et l'intervalle de temps demandés.


## Formats de sorties disponibles

  - text
  - json
  - geocsv

## Utilisation de la requête

Les paramètres de la requête sont joints par une esperluette "&", sans espace (voir les exemples de requêtes). Les valeurs par défaut sont indiquées en majuscules.
Au moins une station ou un réseau doit être précisé.

### /extent usage

    /extent? [channel-options] [date-range-options] [merge-options] [sort-options] [display-options] [format-options] [nodata=404]

    où :

    channel-options      ::  [net=<network>] [sta=<station>] [loc=<location>] [cha=<channel>] [quality=<quality>]
    date-range-options   ::  [starttime=<date|durée>] [endtime=<date|durée>]
    merge-options        ::  [merge=<quality|samplerate|overlap>]
    sort-options         ::  [orderby=<NSLC_TIME_QUALITY_SAMPLERATE|timespancount|timespancount_desc|latestupdate|latestupdate_desc>]
    display-options      ::  [includerestricted=<true|FALSE>] [limit=<number>]
    format-options       ::  [format=<TEXT|geocsv|json|request|zip>]

    les valeurs par défaut sont en majuscules

### /query usage

    /query? [channel-options] [date-range-options] [merge-options] [sort-options] [display-options] [format-options] [nodata=404]

    où :

    channel-options      ::  [net=<network>] [sta=<station>] [loc=<location>] [cha=<channel>] [quality=<quality>]
    date-range-options   ::  (starttime=<date|durée>) (endtime=<date|durée>)
    merge-options        ::  [merge=<quality|samplerate|overlap>] [mergegaps=<number>]
    sort-options         ::  [orderby=<NSLC_TIME_QUALITY_SAMPLERATE|latestupdate|latestupdate_desc>]
    display-options      ::  [includerestricted=<true|FALSE>] [limit=<number>] [show=<latestupdate>]
    format-options       ::  [format=<TEXT|geocsv|json|request|zip>]

    les valeurs par défaut sont en majuscules

## Exemples de requêtes

### avec /extent
<a href="http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01">http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01</a>

<a href="http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&show=latestupdate&orderby=timespancount">http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&show=latestupdate&orderby=timespancount</a>


### avec /query

<a href="http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01">http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01</a>

<a href="http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&merge=samplerate&mergegaps=36000">http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&merge=samplerate&mergegaps=36000</a>

## Descriptions détaillées de chaque paramètre de la requête

### Choix du capteur

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| net[work]  | FR      | Nom du réseau sismique. Accepte les jokers et les listes.                     |
| sta[tion]  | CIEL    | Nom de la station. Accepte les jokers et les listes.                          |
| loc[ation] | 00      | Code de localisation. Utilisez loc=-- pour les codes de localisations vides. Accepte les jokers et les listes. |
| cha[nnel]  | HHZ     | Code de canal. Accepte les jokers et les listes.                              |

#### Jokers et listes d'arguments

  - Jokers : le point d’interrogation __?__ représente n'importe quel caractère unique, alors que l'astérisque __*__ représente zéro caractère ou plus.

  - Listes : plusieurs éléments peuvent être récupérés à l'aide d'une liste séparée par des virgules. Les jokers peuvent être inclus dans la liste.

Par exemple, pour le code des canaux : channel=EH?,BHZ

### Choix de l'intervalle de temps

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| start[time] | 2010-01-10T00:00:00 | Sélectionne la disponibilité des enregistrements à partir de l'heure spécifiée incluse. |
| end[time]  | 2011-02-11T01:00:00 | Sélectionne la disponibilité des enregistrements avant l'heure spécifiée incluse. |

La définition de l'intervalle de temps avec starttime et endtime peut prendre différentes formes :

  - une date, par exemple starttime=2015-08-12T01:00:00
  - une durée en secondes, par exemple endtime=7200
  - le mot-clé "currentutcday" qui signifie minuit de la date du jour (UTC), par exemple starttime=currentutcday

### Options de fusion

| Paramètre       | Exemples   | Discussion                                                                       |
| :-------------- | :--------- | :------------------------------------------------------------------------------- |
| merge           |            | Liste de paramètres séparés par des virgules (exemple merge=quality,samplerate). |
|                 | quality    | Les plages de temps de qualités différentes sont fusionnées.                            |
|                 | samplerate | Les plages de temps de fréquences d'échantillonnage différentes sont fusionnées.        |
|                 | overlap    | Non applicable.                                                                  |

### Options de sortie

| Paramètre  | Exemples | Discussion                                                                                                |
| :--------- | :------- | :-------------------------------------------------------------------------------------------------------- |
| format     | json     | Format de sortie. Valeurs autorisées : text (par défaut, avec les entêtes), geocsv, json, request et zip. |
| includerestricted     | false | Affiche ou non les données restreintes.                                                           |
| limit      | integer  | Limite le nombre de lignes en sortie.                                                                     |
| nodata     | 404      | Code d'état HTTP qui est renvoyé lorsqu'il n'y a pas de données (204 or 404)                              |

### Options de tri

| Paramètre  | Exemples                     | Discussion                                                                      |
| :--------- | :--------------------------- | :------------------------------------------------------------------------------ |
| orderby    |                              | Range les lignes par :                                                          |
|            | nslc_time_quality_samplerate | network, station, location, channel, plage de temps, quality, sample-rate (par défaut) |
|            | latestupdate                 | date de mise à jour (du plus ancien au plus récent), network, station, location, channel, plage de temps, quality, sample-rate |
|            | latestupdate_desc            | date de mise à jour (du plus récent au plus ancien), network, station, location, channel, plage de temps, quality, sample-rate |


### Paramètres additionnels pour la méthode extent

| Paramètre  | Exemples           | Discussion                                                                      |
| :--------- | :----------------- | :------------------------------------------------------------------------------ |
| orderby    |                    | Range les lignes par :                                                          |
|            | timespancount      | nombre de plages de temps (du plus petit au plus grand), network, station, location, channel, plage de temps, quality, sample-rate |
|            | timespancount_desc | nombre de plages de temps (du plus grand au plus petit), network, station, location, channel, plage de temps, quality, sample-rate |


### Paramètres additionnels pour la méthode query

| Paramètre       | Exemple  | Discussion                                                            |
| :-------------- | :------- | :-------------------------------------------------------------------- |
| mergegaps       | 3600     | Les plages de temps qui sont séparées par des gaps plus petits ou égaux à la valeur donnée en secondes sont fusionnées. |
| show            | latestupdate  | Affiche la date de mise à jour des données.                      |

## Requêtes HTTP POST

La forme générale d'une requête POST est un ensemble de paires parameter=value, une par ligne, suivie d'un nombre arbitraire de canaux et d'une fenêtre temporelle optionnelle :

parameter=\<value\> \
parameter=\<value\> \
parameter=\<value\> \
Net Sta Loc Chan [StartTime EndTime] \
Net Sta Loc Chan [StartTime EndTime] \
...

La fenêtre temporelle peut être précisée globalement :

... \
start=2020-10-01T00:00:00 \
end=2020-10-01T00:01:00 \
Net1 Sta1 Loc1 Chan1 \
Net2 Sta2 Loc2 Chan2 \
...

ou par ligne :

... \
Net1 Sta1 Loc1 Chan1 2020-10-01T00:00:00 2020-10-01T00:01:00 \
Net2 Sta2 Loc2 Chan2 2020-10-02T00:00:00 2020-10-02T00:02:00 \
...

La fenêtre temporelle globale peut être utilisée conjointement avec des fenêtres temporelles individuelles. Si aucune information temporelle n’est précisée, la période maximale de disponibilité est utilisée.

## Formats des dates et des heures

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (une heure de 00:00:00 est supposée)

    avec :

    YYYY    :: quatre chiffres de l'année
    MM      :: deux chiffres du mois (01=Janvier, etc.)
    DD      :: deux chiffres du jour du mois (01 à 31)
    T       :: séparateur date-heure
    hh      :: deux chiffres de l'heure (00 à 23)
    mm      :: deux chiffres des minutes (00 à 59)
    ss      :: deux chiffres des secondes (00 à 59)
    ssssss  :: un à six chiffres des microsecondes en base décimale (0 à 999999)

