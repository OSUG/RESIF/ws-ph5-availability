# Webservice ph5-availability

## Description

The ph5-availability web service returns detailed timespan information of what time series is available in the PH5 database.

There are two service query methods:

/extent

Produce a list of available time extents (earliest to latest) for selected channels (network, station, location, channel and quality) and time ranges.

/query

Produce a list of contiguous timespans for selected channels (network, station, location, channel and quality) and time ranges.


## Output format options:

  - text
  - json
  - geocsv

## Query usage

Query parameters are joined by ampersands "&", without blank space (see the sample queries). Default values are uppercase.
At least one station or one network must be specified.

### /extent usage

    /extent? [channel-options] [date-range-options] [merge-options] [sort-options] [display-options] [format-options] [nodata=404]

    where :

    channel-options      ::  [net=<network>] [sta=<station>] [loc=<location>] [cha=<channel>] [quality=<quality>]
    date-range-options   ::  [starttime=<date|duration>] [endtime=<date|duration>]
    merge-options        ::  [merge=<quality|samplerate|overlap>]
    sort-options         ::  [orderby=<NSLC_TIME_QUALITY_SAMPLERATE|timespancount|timespancount_desc|latestupdate|latestupdate_desc>]
    display-options      ::  [includerestricted=<true|FALSE>] [limit=<number>]
    format-options       ::  [format=<TEXT|geocsv|json|request|zip>]

    default values are uppercase

### /query usage

    /query? [channel-options] [date-range-options] [merge-options] [sort-options] [display-options] [format-options] [nodata=404]

    where :

    channel-options      ::  [net=<network>] [sta=<station>] [loc=<location>] [cha=<channel>] [quality=<quality>]
    date-range-options   ::  [starttime=<date|duration>] [endtime=<date|duration>]
    merge-options        ::  [merge=<quality|samplerate|overlap>] [mergegaps=<number>]
    sort-options         ::  [orderby=<NSLC_TIME_QUALITY_SAMPLERATE|latestupdate|latestupdate_desc>]
    display-options      ::  [includerestricted=<true|FALSE>] [limit=<number>] [show=<latestupdate>]
    format-options       ::  [format=<TEXT|geocsv|json|request|zip>]

    default values are uppercase

## Sample queries

### with /extent

<a href="http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01">http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01</a>

<a href="http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&show=latestupdate&orderby=timespancount">http://ph5ws.resif.fr/resifws/availability/1/extent?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&show=latestupdate&orderby=timespancount</a>


### with /query

<a href="http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01">http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01</a>

<a href="http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&merge=samplerate&mergegaps=36000">http://ph5ws.resif.fr/resifws/availability/1/query?net=3C&sta=N02,N08&start=2019-01-01&end=2020-01-01&merge=samplerate&mergegaps=36000</a>

## Detailed descriptions of each query parameter

### Channel options

| Parameter  | Example  | Discussion                                                                       |
| :--------- | :------- | :--------------------------------------------------------------------------------|
| net[work]  | FR       | Seismic network name. Accepts wildcards and lists.                               |
| sta[tion]  | CIEL     | Station name. Accepts wildcards and lists.                                       |
| loc[ation] | 00       | Location code. Use loc=-- for empty location codes. Accepts wildcards and lists. |
| cha[nnel]  | HHZ      | Channel Code. Accepts wildcards and lists.                                       |

#### Wildcards and lists

  - Wildcards: the question mark __?__ represents any single character, while the asterisk __*__ represents zero or more characters.

  - List: multiple items may be retrieved using a comma-separated list. Wildcards may be included in the list.

For example, with channel codes: channel=EH?,BHZ

### Date-range options

| Parameter   | Example             | Discussion                                                          |
| :---------- | :------------------ | :------------------------------------------------------------------ |
| start[time] | 2010-01-10T00:00:00 | Selects timespan information on or after the specified start time. |
| end[time]   | 2011-02-11T01:00:00 | Selects timespan information on or before the specified end time.  |

The definition of the time interval may take different forms:

  - a calendar dates, for example starttime=2015-08-12T01:00:00
  - duration in seconds, for example endtime=7200
  - the key word "currentutcday" which means midnight of today’s date (UTC time), for example starttime=currentutcday

### Merge options

| Parameters      | Examples   | Discussion                                                            |
| :-------------- | :--------- | :-------------------------------------------------------------------- |
| merge           |            | Comma separated list (example merge=quality,samplerate).              |
|                 | quality    | The timespans of data with differing quality are grouped together.      |
|                 | samplerate | The timespans of data with differing sample rates are grouped together. |
|                 | overlap    | Not applicable.                                                       |

### Output options

| Parameters  | Examples | Discussion                                                                           |
| :---------- | :------- | :----------------------------------------------------------------------------------- |
| format      | json    | Specify the output format. Accepted values are text (the default), json, request and zip. |
| includerestricted | false | Display or not restricted data.                                                   |
| limit       | integer  | Limits output to this many rows.                                                     |
| nodata      | 404      | Specify which HTTP status code is returned when no data is found (204 or 404)        |

### Sort options

| Parameters | Examples                     | Discussion                                                                      |
| :--------- | :--------------------------- | :------------------------------------------------------------------------------ |
| orderby    |                              | Sort rows by:                                                                   |
|            | nslc_time_quality_samplerate | network, station, location, channel, time-range, quality, sample-rate (default) |
|            | latestupdate                 | update-date (past to present), network, station, location, channel, time-range, quality, sample-rate |
|            | latestupdate_desc            | update-date (present to past), network, station, location, channel, time-range, quality, sample-rate |


### Additional parameters for the extent method

| Parameters | Examples           | Discussion                                                                      |
| :--------- | :----------------- | :------------------------------------------------------------------------------ |
| orderby    |                    | Sort rows by:                                                                   |
|            | timespancount      | number of timespans (small to large), network, station, location, channel, time-range, quality, sample-rate |
|            | timespancount_desc | number of timespans (large to small), network, station, location, channel, time-range, quality, sample-rate |


### Additional parameters for the query method

| Parameters      | Example  | Discussion                                                            |
| :-------------- | :------- | :-------------------------------------------------------------------- |
| mergegaps       | 3600     | The timespans which are separated by gaps smaller or equal than the given value (in seconds) are merged together. |
| show            | latestupdate  | Display the last date of data update.             |

## HTTP POST queries

The general form of a POST is parameter=value pairs, one per line, followed by an arbitrary number of channels and, optionally, time window selection lines:

parameter=\<value\> \
parameter=\<value\> \
parameter=\<value\> \
Net Sta Loc Chan [StartTime EndTime] \
Net Sta Loc Chan [StartTime EndTime] \
...

Start time and end times can be specified globally, such as

... \
start=2020-10-01T00:00:00 \
end=2020-10-01T00:01:00\
Net1 Sta1 Loc1 Chan1 \
Net2 Sta2 Loc2 Chan2 \
...

or per line:

... \
Net1 Sta1 Loc1 Chan1 2020-10-01T00:00:00 2020-10-01T00:01:00 \
Net2 Sta2 Loc2 Chan2 2020-10-02T00:00:00 2020-10-02T00:02:00 \
...

If not given, the start and end times default to the fully available time range. Additionally, global time ranges can be mixed with individual time ranges.

## Date and time formats

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (a time of 00:00:00 is assumed)

    where:

    YYYY    :: four-digit year
    MM      :: two-digit month (01=January, etc.)
    DD      :: two-digit day (01 through 31)
    T       :: date-time separator
    hh      :: two-digit hour (00 through 23)
    mm      :: two-digit number of minutes (00 through 59)
    ss      :: two-digit number of seconds (00 through 59)
    ssssss  :: one to six-digit number of microseconds (0 through 999999)

