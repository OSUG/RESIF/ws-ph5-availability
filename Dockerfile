FROM python:3.9-slim
MAINTAINER RESIF DC <resif-dc@univ-grenoble-alpes.fr>
RUN pip install --no-cache-dir gunicorn
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt

WORKDIR /appli
COPY start.py config.py ./
COPY apps ./apps/
COPY templates ./templates/
COPY static ./static/
USER 1000

CMD ["/bin/bash", "-c", "gunicorn --bind 0.0.0.0:8000 start:app"]
