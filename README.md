# Webservice ph5-availability

## Play around with docker

```
docker build -t ws-ph5-availability .
docker run --rm -e RUNMODE=test -p 8000:8000 --name ws-ph5-availability ws-ph5-availability
```

Then :

```
wget -O - http://localhost:8000/1/application.wadl
```

Run it in debug mode with flask:

```
RUNMODE=test FLASK_APP=start.py flask run
```

## RUNMODE builtin values

  * `production`
  * `test`

## Authors (dc@resif.fr)

  * Jérôme Touvier
  * Philippe Bollard (Maintainer)
